#!/usr/bin/env python

from nn.model import Model
from nn.layers import Dense
from nn.layers import Sigmoid, ReLU, Softmax
from nn.criterion import CrossEntropy, MSE
from nn.data import Dataset
from nn.data import get_mnist, get_spin, get_sep
from nn.util import plot_decision_boundary
from nn.util import train
from nn.util import check_gradients

import numpy as np


def construct_model(task, batch_size, num_features, num_classes):
    if task == 'mnist':
        model = Model(batch_size, num_features)
        model.add(Dense(300))
        model.add(Sigmoid())
        model.add(Dense(num_classes))
        model.add(Softmax())
        criterion = CrossEntropy()
        return model, criterion
    elif task == 'sep' or task == 'spin':
        model = Model(batch_size, num_features)
        model.add(Dense(256))
        model.add(ReLU())
        model.add(Dense(128))
        model.add(ReLU())
        model.add(Dense(num_classes))
        model.add(Softmax())
        criterion = MSE()
        return model, criterion
    elif task == 'check':
        model = Model(batch_size, num_features)
        model.add(Dense(128))
        model.add(Sigmoid())
        model.add(Dense(num_classes))
        model.add(Sigmoid())
        criterion = MSE()
        return model, criterion
    else:
        print "Undefined task: {}".format(task)
        exit()


def load_features_labels(task, batch_size):
    if task == 'mnist':
        train_features, train_labels, val_features, val_labels = get_mnist('/tmp/mnist')
        train_features = np.divide(train_features.reshape(-1, 28 * 28).astype(np.float32), 255.0)
        val_features = np.divide(val_features.reshape(-1, 28 * 28).astype(np.float32), 255.0)
        test_features, test_labels = val_features, val_labels
    elif task == 'sep' or task == 'check':
        train_features, train_labels = get_sep(points_per_class=batch_size * 16)
        val_features, val_labels = get_sep(points_per_class=batch_size * 16)
        test_features, test_labels = get_sep(points_per_class=batch_size * 16)
    elif task == 'spin':
        train_features, train_labels = get_spin(points_per_class=batch_size * 32)
        val_features, val_labels = get_spin(points_per_class=batch_size * 8)
        test_features, test_labels = get_spin(points_per_class=batch_size * 8)
    else:
        print "Undefined task: {}".format(task)
        train_features, train_labels = None, None
        val_features, val_labels = None, None
        test_features, test_labels = None, None
        exit()
    return train_features, train_labels, val_features, val_labels, test_features, test_labels


def draw_optional(task, batch_size, model, test_features, test_labels):
    if task != 'sep' and task != 'spin':
        return

    def predict(features, labels):
        dataset = Dataset(features, labels, batch_size)
        predictions = []
        for batch_index in range(dataset.num_batches()):
            features_batch, _ = dataset.get_batch(batch_index)
            model.forward(features_batch)
            label_batch = np.argmax(model.get_output(), axis=-1)
            predictions.append(label_batch)
        predictions = np.concatenate(predictions)
        return predictions

    plot_decision_boundary("decision-boundary.png", test_features, test_labels, predict)


def main(task, batch_size, num_epoch):
    train_features, train_labels, val_features, val_labels, test_features, test_labels = \
        load_features_labels(task, batch_size)
    train_dataset = Dataset(train_features, train_labels, batch_size)
    val_dataset = Dataset(val_features, val_labels, batch_size)
    # test_dataset = Dataset(test_features, test_labels, batch_size)
    model, criterion = construct_model(
        task,
        train_dataset.batch_size(),
        train_dataset.num_features(),
        train_dataset.num_classes()
    )
    train(model, criterion, train_dataset, val_dataset, num_epoch)
    draw_optional(task, batch_size, model, test_features, test_labels)
    if task == 'check':
        check_gradients(model, criterion, train_dataset)

if __name__ == '__main__':
    import argparse
    ap = argparse.ArgumentParser(description='Neural Network')
    ap.add_argument('--task', required=True, help='Task name: "mnist", "sep" or "spin"')
    ap.add_argument('--batch', required=True, help='Batch size')
    ap.add_argument('--epoch', required=True, help='Number of epoch')
    args = vars(ap.parse_args())
    main(args['task'], int(args['batch']), int(args['epoch']))
