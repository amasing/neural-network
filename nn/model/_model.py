from ..layers._core import Layer
from ..util import plot_hist

import numpy as np
import os


class Model(object):
    def __init__(self, batch_size, num_features):
        self._layers = []
        self._data_tensors = []
        self._err_tensors = []
        self._add_tensors((batch_size, num_features))

    def _add_tensors(self, shape):
        data_tensor = np.zeros(shape)
        err_tensor = np.zeros(shape)
        self._data_tensors.append(data_tensor)
        self._err_tensors.append(err_tensor)

    def _get_last_tensors(self):
        return self._data_tensors[-1], self._err_tensors[-1]

    def add(self, layer):
        assert issubclass(type(layer), Layer)
        data_in, err_out = self._get_last_tensors()
        self._add_tensors(layer.calc_output_shape(data_in.shape))
        data_out, err_in = self._get_last_tensors()
        layer.initialize(data_in, data_out, err_in, err_out)
        self._layers.append(layer)

    def forward(self, data):
        assert data.shape == self._data_tensors[0].shape
        np.copyto(self._data_tensors[0], data)
        for layer in self._layers:
            layer.forward()

    def backward(self, err):
        assert err.shape == self._err_tensors[-1].shape
        np.copyto(self._err_tensors[-1], err)
        for layer in self._layers[::-1]:
            layer.backward()

    def calc_grads(self, lambda_):
        for layer in self._layers:
            layer.calc_grads(lambda_)

    def sgd_step(self, alpha):
        for layer in self._layers:
            layer.update_params(alpha)

    def get_output(self):
        return self._data_tensors[-1]

    def create_layers_stats(self, directory):
        if not os.path.exists(directory):
            os.mkdir(directory)
        for i in range(len(self._layers)):
            layer_name = self._layers[i].get_name()
            data = self._data_tensors[i]
            err = self._err_tensors[i + 1]
            name = "{}_{}_data.png".format(i, layer_name)
            path = os.path.join(directory, name)
            plot_hist(path, "Input Data Histogram", data.reshape(-1))
            name = "{}_{}_err.png".format(i, layer_name)
            path = os.path.join(directory, name)
            plot_hist(path, "Input Error Histogram", err.reshape(-1))

    def get_params(self):
        all_params = []
        for layer in self._layers:
            all_params.extend(layer.get_params())
        return all_params

    def get_grads(self):
        all_grads = []
        for layer in self._layers:
            all_grads.extend(layer.get_grads())
        return all_grads
