import numpy as np


class Dataset(object):
    def __init__(self, features, targets, batch_size, encode_one_hot=True, num_classes=None):
        assert features.shape[0] == targets.shape[0]
        if encode_one_hot:
            assert np.ndim(targets) == 1
            assert (targets.dtype == np.int32 or targets.dtype == np.uint8)
        else:
            assert np.ndim(targets) == 2
            assert targets.dtype == np.float32
        if encode_one_hot:
            if num_classes is None:
                num_classes = np.max(targets) + 1
            self._num_classes = num_classes
        self._features = features
        self._targets = targets
        self._encode_one_hot = encode_one_hot
        self._num_batches = features.shape[0] / batch_size
        self._batch_size = batch_size

    def batch_size(self):
        return self._batch_size

    def num_batches(self):
        return self._num_batches

    def num_classes(self):
        return self._num_classes

    def num_features(self):
        return self._features.shape[-1]

    def get_batch(self, batch_index):
        assert batch_index < self._num_batches
        offset = batch_index * self._batch_size
        if self._encode_one_hot:
            labels = self._targets[offset: offset + self._batch_size]
            one_hot_encoded = np.zeros((self._batch_size, self._num_classes), dtype=np.float32)
            for i in range(self._batch_size):
                one_hot_encoded[i, labels[i]] = 1.0
            return (
                self._features[offset: offset + self._batch_size],
                one_hot_encoded
            )
        else:
            return (
                self._features[offset: offset + self._batch_size],
                self._targets[offset: offset + self._batch_size]
            )
