from _dataset import Dataset
from _mnist import get_mnist
from _simple import get_sep, get_spin

__all__ = ['Dataset', 'get_mnist', 'get_sep', 'get_spin']