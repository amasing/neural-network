import numpy as np


def get_sep(points_per_class=128):
    features_0 = np.random.normal(size=(points_per_class, 2)) + np.array([-3.0, -3.0])
    features_1 = np.random.normal(size=(points_per_class, 2)) + np.array([3.0, 4.0])
    features_2 = np.random.normal(size=(points_per_class, 2)) + np.array([-1.0, 4.0])
    features = np.concatenate([features_0, features_1, features_2]).astype(np.float32)
    labels = np.array([0] * len(features_0) + [1] * len(features_1) + [2] * len(features_2)).astype(np.int32)
    permutation = np.random.permutation(len(labels))
    return features[permutation], labels[permutation]


def get_spin(points_per_class=128, num_classes=3):
    features = np.zeros((points_per_class * num_classes, 2))
    labels = np.zeros(points_per_class * num_classes, dtype=np.int32)
    for label in xrange(num_classes):
        ix = range(points_per_class * label, points_per_class * (label + 1))
        radius = np.linspace(0.0, 1, points_per_class)
        linear = np.linspace(label * 4, (label + 1) * 4, points_per_class) + np.random.randn(points_per_class) * 0.2
        features[ix] = np.c_[radius * np.sin(linear), radius * np.cos(linear)]
        labels[ix] = label
    permutation = np.random.permutation(len(labels))
    return features[permutation], labels[permutation]