import numpy as np
import urllib2
import struct
from array import array
import gzip
import os


def get_mnist(target_directory):
    def download_data(directory):
        filenames_to_download = [
            "train-images-idx3-ubyte.gz",
            "train-labels-idx1-ubyte.gz",
            "t10k-images-idx3-ubyte.gz",
            "t10k-labels-idx1-ubyte.gz"
        ]
        base_url = "http://yann.lecun.com/exdb/mnist/"
        if not os.path.exists(directory):
            os.mkdir(directory)
        for name_to_download in filenames_to_download:
            target_path = os.path.join(directory, name_to_download)
            url = os.path.join(base_url, name_to_download)
            if os.path.exists(target_path):
                continue
            print "Downloading {} -> {}".format(url, target_path)
            web_file = urllib2.urlopen(url)
            with open(target_path, "wb") as local_file:
                local_file.write(web_file.read())
            web_file.close()

    def load_images(gz_file_path):
        print "Loading {}".format(gz_file_path)
        with gzip.open(gz_file_path, 'rb') as f:
            magic, size, rows, cols = struct.unpack(">IIII", f.read(16))
            assert magic == 2051, 'Magic number mismatch, expected 2051, got {}'.format(magic)
            image_data = array("B", f.read())
            image_tensor = np.array(image_data).reshape((size, 28, 28)).astype(np.ubyte)
        return image_tensor

    def load_labels(gz_file_path):
        print "Loading {}".format(gz_file_path)
        with gzip.open(gz_file_path, 'rb') as f:
            magic, size = struct.unpack(">II", f.read(8))
            assert magic == 2049, 'Magic number mismatch, expected 2051, got {}'.format(magic)
            labels_data = array("B", f.read())
            labels_tensor = np.array(labels_data).astype(np.int32)
        return labels_tensor

    download_data(target_directory)
    filenames = os.listdir(target_directory)
    train_images, train_labels = None, None
    test_images, test_labels = None, None
    for name in filenames:
        path = os.path.join(target_directory, name)
        if "images" in name:
            if "train" in name:
                train_images = load_images(path)
            if "t10k" in name:
                test_images = load_images(path)
        if "labels" in name:
            if "train" in name:
                train_labels = load_labels(path)
            if "t10k" in name:
                test_labels = load_labels(path)
    return train_images, train_labels, test_images, test_labels
