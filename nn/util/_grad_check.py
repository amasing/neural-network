import numpy as np
from tqdm import tqdm


def check_gradients(model, criterion, dataset):
    params = model.get_params()
    grads = model.get_grads()
    xs, ys = dataset.get_batch(0)
    eps = 1e-6
    all_grads = []
    all_diffs = []
    for p, g in zip(params, grads)[::-1]:
        p = p.reshape(-1)
        g = g.reshape(-1)
        for i in tqdm(range(p.shape[0])):
            model.forward(xs)
            err = criterion.calc_err(model.get_output(), ys)
            model.backward(err)
            model.calc_grads(0.0)
            p[i] += eps
            model.forward(xs)
            loss_plus = criterion.calc_loss(model.get_output(), ys)
            p[i] -= 2.0 * eps
            model.forward(xs)
            loss_minus = criterion.calc_loss(model.get_output(), ys)
            p[i] += eps
            all_diffs.append((loss_plus - loss_minus) / (2 * eps))
            all_grads.append(g[i])

    print np.sqrt(np.sum(np.square(np.array(all_grads) - np.array(all_diffs))))
