import numpy as np
import tqdm


def train(model, criterion, train_dataset, val_dataset, num_epoch):
    from sklearn.metrics import confusion_matrix
    steps, val_losses, test_losses, test_accuracies = [], [], [], []
    for epoch in range(num_epoch):
        print "-" * 30 + "  epoch #{}  ".format(epoch + 1) + "-" * 30
        num_batches = train_dataset.num_batches()
        iterator = tqdm.tqdm(range(num_batches))
        epoch_train_losses, epoch_test_losses = [], []
        for batch_index in iterator:
            features_batch, targets_batch = train_dataset.get_batch(batch_index)
            model.forward(features_batch)
            model.backward(criterion.calc_err(model.get_output(), targets_batch))
            model.calc_grads(0.01)
            model.sgd_step(0.5)
            epoch_train_losses.append(criterion.calc_loss(model.get_output(), targets_batch))

        model.create_layers_stats("epoch_{}".format(epoch + 1))

        num_batches = val_dataset.num_batches()
        y_true, y_pred = [], []
        for batch_index in range(num_batches):
            features_batch, targets_batch = val_dataset.get_batch(batch_index)
            model.forward(features_batch)
            epoch_test_losses.append(criterion.calc_loss(model.get_output(), targets_batch))
            predicted_batch = np.argmax(model.get_output(), axis=-1)
            labels_batch = np.argmax(targets_batch, axis=-1)
            y_pred.append(predicted_batch)
            y_true.append(labels_batch)
        y_true = np.concatenate(y_true)
        y_pred = np.concatenate(y_pred)
        cm = confusion_matrix(y_true, y_pred)

        val_losses.append(np.mean(epoch_train_losses))
        test_losses.append(np.mean(np.mean(epoch_test_losses)))
        test_accuracies.append(np.mean(y_true == y_pred) * 100.0)
        steps.append(epoch * num_batches)
        print "Train loss: {}".format(val_losses[-1])
        print "Test loss: {}".format(test_losses[-1])
        print "Accuracy: {:.2f}%".format(test_accuracies[-1])
        print "Confusion matrix:"
        print cm
    return model
