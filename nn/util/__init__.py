from _visualization import plot_decision_boundary
from _visualization import plot_hist
from _learning import train
from _grad_check import check_gradients

__all__ = ['plot_decision_boundary', 'plot_hist', 'train', 'check_gradients']