import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np


def plot_hist(path, title, values):
    sigma = np.std(values)
    hist, bins = np.histogram(values, bins=50, range=(-2.0 * sigma, 2.0 * sigma))
    width = 0.8 * (bins[1] - bins[0])
    center = (bins[:-1] + bins[1:]) / 2
    plt.clf()
    plt.bar(center, hist, align='center', width=width)
    plt.title(title)
    plt.xlabel("value")
    plt.ylabel("frequency")
    plt.savefig(path, dpi=80)


def plot_decision_boundary(path, features, labels, predict_function):
    x_min, x_max = features[:, 0].min() - 0.5, features[:, 0].max() + 0.5
    y_min, y_max = features[:, 1].min() - 0.5, features[:, 1].max() + 0.5
    xx, yy = np.meshgrid(
        np.arange(x_min, x_max, (x_max - x_min) / 512),
        np.arange(y_min, y_max, (y_max - y_min) / 512)
    )
    grid_data = np.vstack([xx.ravel(), yy.ravel()]).T
    dummy_labels = np.array([0] * grid_data.shape[0], dtype=np.int32)
    z = predict_function(grid_data, dummy_labels).reshape(xx.shape)
    plt.clf()
    plt.contourf(xx, yy, z, cmap=plt.cm.Spectral)
    plt.scatter(features[:, 0], features[:, 1], c=labels, cmap=plt.cm.Spectral)
    plt.title("Decision boundary")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.savefig(path, dpi=80)
