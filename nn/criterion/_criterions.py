import numpy as np


class MSE:
    def __init__(self):
        pass

    @staticmethod
    def calc_loss(output, target):
        return np.mean(np.square(output - target))

    @staticmethod
    def calc_err(output, target):
        return output - target


class CrossEntropy:
    def __init__(self):
        pass

    @staticmethod
    def calc_loss(output, target):
        return -np.sum(np.multiply(target, np.log(np.clip(output, 1e-6, 1.0))))

    @staticmethod
    def calc_err(output, target):
        return -np.divide(target, np.clip(output, 1e-6, 1.0))
