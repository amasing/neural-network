from _criterions import CrossEntropy
from _criterions import MSE

__all__ = ['CrossEntropy', 'MSE']