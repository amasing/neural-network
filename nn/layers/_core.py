import numpy as np


class Layer(object):
    def __init__(self, name):
        self._data_in = None
        self._data_out = None
        self._err_in = None
        self._err_out = None
        self._name = name

    def initialize(self, data_in, data_out, err_in, err_out):
        expected_shape = self.calc_output_shape(data_in.shape)
        assert data_out.shape == expected_shape, "{} != {}".format(data_out.shape, expected_shape)
        assert data_in.shape == err_out.shape, "{} != {}".format(data_in.shape, err_out.shape)
        assert data_out.shape == err_in.shape, "{} != {}".format(data_out.shape, err_in.shape)
        self._data_in = data_in
        self._data_out = data_out
        self._err_in = err_in
        self._err_out = err_out

    def get_name(self):
        return self._name

    def calc_output_shape(self, input_shape):
        raise Exception("calc_output_shape() method was not implemented")

    def forward(self):
        raise Exception("forward() method was not implemented")

    def backward(self):
        raise Exception("backward() method was not implemented")

    def calc_grads(self, lambda_):
        pass

    def update_params(self, alpha):
        pass

    def get_params(self):
        return []

    def get_grads(self):
        return []

    @staticmethod
    def create_weights(shape, const=None):
        if np.ndim(shape) == 2:
            n_in, n_out = shape
        else:
            n_in = n_out = shape[0]
        if const is not None:
            return np.ones(shape, dtype=np.float32) * const
        return np.random.normal(scale=(2.0 / (n_in + n_out)), size=shape)
