from _core import Layer

import numpy as np


class ReLU(Layer):
    def __init__(self):
        Layer.__init__(self, "relu")

    def calc_output_shape(self, input_shape):
        return input_shape

    def initialize(self, data_in, data_out, err_in, err_out):
        Layer.initialize(self, data_in, data_out, err_in, err_out)

    def forward(self):
        np.maximum(self._data_in, 0.0, out=self._data_out)

    def backward(self):
        np.multiply(self._err_in, self._data_in > 0.0, out=self._err_out)


class Sigmoid(Layer):
    def __init__(self):
        Layer.__init__(self, "sigmoid")

    def calc_output_shape(self, input_shape):
        return input_shape

    def initialize(self, data_in, data_out, err_in, err_out):
        Layer.initialize(self, data_in, data_out, err_in, err_out)

    def forward(self):
        np.negative(self._data_in, out=self._data_out)
        np.exp(self._data_out, out=self._data_out)
        np.add(self._data_out, 1.0, out=self._data_out)
        np.reciprocal(self._data_out, out=self._data_out)

    def backward(self):
        np.subtract(1.0, self._data_out, out=self._err_out)
        np.multiply(self._data_out, self._err_out, out=self._err_out)
        np.multiply(self._err_in, self._err_out, out=self._err_out)


class Tanh(Layer):
    def __init__(self):
        Layer.__init__(self, "tanh")

    def calc_output_shape(self, input_shape):
        return input_shape

    def initialize(self, data_in, data_out, err_in, err_out):
        Layer.initialize(self, data_in, data_out, err_in, err_out)

    def forward(self):
        np.tanh(self._data_in, out=self._data_out)

    def backward(self):
        np.square(self._data_in, out=self._err_out)
        np.subtract(1.0, self._err_out, out=self._err_out)
        np.multiply(self._err_in, self._err_out, out=self._err_out)


class Softmax(Layer):
    def __init__(self):
        Layer.__init__(self, "softmax")

    def calc_output_shape(self, input_shape):
        return input_shape

    def initialize(self, data_in, data_out, err_in, err_out):
        Layer.initialize(self, data_in, data_out, err_in, err_out)

    def forward(self):
        np.exp(self._data_in, out=self._data_out)
        np.divide(self._data_out, np.sum(self._data_out, axis=-1)[np.newaxis].T, out=self._data_out)

    def backward(self):
        tmp = np.sum(np.multiply(self._err_in, self._data_out), axis=-1, keepdims=True)
        np.subtract(self._err_in, tmp, out=self._err_out)
        np.multiply(self._err_out, self._data_out, out=self._err_out)
