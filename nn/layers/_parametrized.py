from _core import Layer

import numpy as np


class Dense(Layer):
    def __init__(self, num_out, bias=True):
        Layer.__init__(self, "dense")
        self._outputs_number = num_out
        self._weights = None
        self._weights_grads = None
        self._with_bias = bias
        if self._with_bias:
            self._biases = None
            self._biases_grads = None

    def calc_output_shape(self, input_shape):
        assert np.ndim(input_shape) != 2, "Dense layer accepts rank 2 tensor"
        num_examples = input_shape[0]
        num_out = self._outputs_number
        return num_examples, num_out

    def initialize(self, data_in, data_out, err_in, err_out):
        Layer.initialize(self, data_in, data_out, err_in, err_out)
        num_in = data_in.shape[-1]
        num_out = self._outputs_number
        self._weights = Layer.create_weights((num_in, num_out))
        self._weights_grads = np.zeros_like(self._weights)
        if self._with_bias:
            self._biases = Layer.create_weights((num_out,), const=0.0)
            self._biases_grads = np.zeros_like(self._biases)

    def forward(self):
        np.dot(self._data_in, self._weights, out=self._data_out)
        if self._with_bias:
            np.add(self._data_out, self._biases, out=self._data_out)

    def backward(self):
        np.dot(self._err_in, self._weights.T, out=self._err_out)

    def calc_grads(self, lambda_):
        batch_size = self._data_in.shape[0]
        np.multiply(self._weights, lambda_, out=self._weights_grads)
        for k in range(batch_size):
            tmp = np.outer(self._data_in[k].T, self._err_in[k])
            np.add(self._weights_grads, tmp, out=self._weights_grads)
        np.divide(self._weights_grads, batch_size, out=self._weights_grads)
        if self._with_bias:
            np.multiply(self._biases, lambda_, out=self._biases_grads)
            for k in range(batch_size):
                np.add(self._biases_grads, self._err_in[k], out=self._biases_grads)
            np.divide(self._biases_grads, batch_size, out=self._biases_grads)

    def update_params(self, alpha):
        np.subtract(self._weights, alpha * self._weights_grads, out=self._weights)
        if self._with_bias:
            np.subtract(self._biases, alpha * self._biases_grads, out=self._biases)

    def get_params(self):
        return [self._weights, self._biases]

    def get_grads(self):
        return [self._weights_grads, self._biases_grads]
